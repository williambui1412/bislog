var app = app || {};

var spBreak = 767;

app.init = function () {

  app.jsDragWidth();
  app.jsTab();
  app.jsSetting();
  app.jsHeightGutter();
  app.jsActive();

};

app.isMobile = function () {

  return window.matchMedia('(max-width: ' + spBreak + 'px)').matches;

};

app.isOldIE = function () {

  return $('html.ie9').length || $('html.ie10').length;

};

app.jsDragWidth = function(){
    Split(['#sidebar', '#main'], {
      sizes: [50, 50],
      gutter: function (index, direction) {
          var gutter = document.createElement('div');
          gutter.className = 'gutter gutter-' + direction
          return gutter
      },
      gutterSize: 2,
    })
}

app.jsTab = function() {
  $("a.js-tab").click(function(event) {
    event.preventDefault();
    $(this).parent().addClass("active");
    $(this).parent().siblings().removeClass("active");
    var tab = $(this).attr("href");
    var tab_content = $(this).attr("data-tab");
    $("." + tab_content).not(tab).css("display", "none");
    $(tab).fadeIn("fast");
  });
};

app.jsSetting = function(){
  $('.js-setting').click(function(){
    $(this).toggleClass('active');
  });
};

app.jsHeightGutter = function(){
  var height_gutter = $('.js-height-main').outerHeight();
  $('.gutter').css("height" , height_gutter)
};

app.jsActive = function(){
  $('.js-toggle-active').click(function(){
    $(this).toggleClass('active');
  });
};

$(function () {

  app.init();

});